(function(){
  let viewMenu = document.getElementById('m-menu-link'),
      blackScreen = document.getElementById('m-menu'),
    closeMenu = document.getElementById('close-menu')
      
  
  viewMenu.onclick = () => {
    blackScreenActiveFunc('active');
  }

  closeMenu.onclick = () => {
    blackScreenActiveFunc('disable');
  }


  let blackScreenActiveFunc = (word) => {
    if(word === 'active'){
      blackScreen.style.transform = 'translateX(0)';
     
    }
    else if (word === 'disable'){
    
      
      
  blackScreen.style.transform = 'translateX(100%)';
      
    }
  }
})();