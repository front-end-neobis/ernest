let trueVariantCountForGreenOrRedRoundBlock = 0;
let numberForTest = 0;
let trueAnswer = 0;

let trueVariant = (e) => {


  let titles = ['What is the name of the youngest of the sons of Starks ?',
                'Whose daughter is daneris?',
                'Where is the Eagles Nest ? ',
 
                'On the emblem of the Starks the wolf is depicted. And what is the motto of this house?'
                ];
  let titlePlace = document.getElementById('Quzzes-title');
  let nextButton = document.getElementById('next');
  let bool = false;
  let el = document.getElementById(e.id);
  let progressBlocks = document.getElementsByClassName('w50f-progress-for-around');
  let progressLines = document.getElementsByClassName('w50f-progress-for-line');
  let numberOfTest = document.getElementById('numberOfTest');
  let wrap = document.getElementById('varWrap');
  let testWrap = document.getElementById('test-wrap');
  let succesPhoto = document.getElementById('succes-photo');
  
  let variantOfChoise = [`
   
  <div id="${getRandomInt(10,10000)}" class="w50-button all-center true" onclick="trueVariant(this)">
  <div class="w50-button-fot-dot all-center">
    <p class="aroundBlock"></p>
  </div>
   
  <div class="w50-button-for-text all-center">
       Riccon
  </div>
</div>

<div id="${getRandomInt(10,10000)}" class="w50-button all-center" onclick="trueVariant(this)">
    <div class="w50-button-fot-dot all-center">
      <p class="aroundBlock"></p>
    </div>
    <div class="w50-button-for-text all-center">
       Jon
    </div>
  </div>

<div id="${getRandomInt(10,10000)}" class="w50-button all-center" onclick="trueVariant(this)">
  <div class="w50-button-fot-dot all-center">
    <p class="aroundBlock"></p>
  </div>
  <div class="w50-button-for-text all-center">
     Bran 
  </div>
</div>

<div id="${getRandomInt(10,10000)}" class="w50-button all-center" onclick="trueVariant(this)">
  <div class="w50-button-fot-dot all-center">
    <p class="aroundBlock"></p>
  </div>
  <div class="w50-button-for-text all-center">
     Tony 
  </div>
</div>
  `,
  `
  <div id="${getRandomInt(10,10000)}" class="w50-button all-center" onclick="trueVariant(this)">
  <div class="w50-button-fot-dot all-center">
    <p class="aroundBlock"></p>
  </div>
  <div class="w50-button-for-text all-center">
       Ned 
  </div>
</div>

<div id="${getRandomInt(10,10000)}" class="w50-button all-center" onclick="trueVariant(this)">
    <div class="w50-button-fot-dot all-center">
      <p class="aroundBlock"></p>
    </div>
    <div class="w50-button-for-text all-center">
      Stanis
    </div>
  </div>

<div id="${getRandomInt(10,10000)}" class="w50-button all-center true" onclick="trueVariant(this)">
  <div class="w50-button-fot-dot all-center">
    <p class="aroundBlock"></p>
  </div>
  <div class="w50-button-for-text all-center">
    Aires
  </div>
</div>

<div id="${getRandomInt(10,10000)}" class="w50-button all-center" onclick="trueVariant(this)">
  <div class="w50-button-fot-dot all-center">
    <p class="aroundBlock"></p>
  </div>
  <div class="w50-button-for-text all-center">
     Tirion
  </div>
</div>
  `,`
  <div id="${getRandomInt(10,10000)}" class="w50-button all-center" onclick="trueVariant(this)">
  <div class="w50-button-fot-dot all-center">
    <p class="aroundBlock"></p>
  </div>
  <div class="w50-button-for-text all-center">
     Сasterly cliff
  </div>
</div>

<div id="${getRandomInt(10,10000)}" class="w50-button all-center true" onclick="trueVariant(this)">
    <div class="w50-button-fot-dot all-center">
      <p class="aroundBlock"></p>
    </div>
    <div class="w50-button-for-text all-center">
       Arren Valley
    </div>
  </div>

<div id="${getRandomInt(10,10000)}" class="w50-button all-center" onclick="trueVariant(this)">
  <div class="w50-button-fot-dot all-center">
    <p class="aroundBlock"></p>
  </div>
  <div class="w50-button-for-text all-center">
      Winterfell
  </div>
</div>

<div id="${getRandomInt(10,10000)}" class="w50-button all-center" onclick="trueVariant(this)">
  <div class="w50-button-fot-dot all-center">
    <p class="aroundBlock"></p>
  </div>
  <div class="w50-button-for-text all-center">
     Dorn   
  </div>
</div>
  `,`
  <div id="${getRandomInt(10,10000)}" class="w50-button all-center" onclick="trueVariant(this)">
  <div class="w50-button-fot-dot all-center">
    <p class="aroundBlock"></p>
  </div>
  <div class="w50-button-for-text all-center">
     dead can not die
  </div>
</div>

<div id="${getRandomInt(10,10000)}" class="w50-button all-center" onclick="trueVariant(this)">
    <div class="w50-button-fot-dot all-center">
      <p class="aroundBlock"></p>
    </div>
    <div class="w50-button-for-text all-center">
      Flame and blood
    </div>
  </div>

<div id="${getRandomInt(10,10000)}" class="w50-button all-center true" onclick="trueVariant(this)">
  <div class="w50-button-fot-dot all-center">
    <p class="aroundBlock"></p>
  </div>
  <div class="w50-button-for-text all-center">
    Winter is coming   
  </div>
</div>

<div id="${getRandomInt(10,10000)}" class="w50-button all-center" onclick="trueVariant(this)">
  <div class="w50-button-fot-dot all-center">
    <p class="aroundBlock"></p>
  </div>
  <div class="w50-button-for-text all-center">
    Honor is above all
  </div>
</div>
  `,];

  if(e.className === 'w50-button all-center true' && trueVariantCountForGreenOrRedRoundBlock === 0){
    trueAnswer++;
    bool = true;
    trueVariantCountForGreenOrRedRoundBlock++;
    let aroundBlockWrap = el.getElementsByClassName('w50-button-fot-dot')[0];
    let aroundBlock = aroundBlockWrap.getElementsByClassName('aroundBlock')[0];
    aroundBlock.style.background = '#00FF00';
    numberForTest = numberForTest + 1;
    progressBlocks[numberForTest - 1].classList.remove('w50f-disable');
   if(numberForTest <= 4) {
      progressLines[numberForTest - 1].classList.remove('w50f-disable');
      progressLines[numberForTest - 1].classList.add('w50f-enable');
    }
   progressBlocks[numberForTest - 1].classList.add('w50f-enable');
    
  }
  if(trueVariantCountForGreenOrRedRoundBlock === 0){
    bool = true;
    trueVariantCountForGreenOrRedRoundBlock++;
    let aroundBlockWrap = el.getElementsByClassName('w50-button-fot-dot')[0];
    let aroundBlock = aroundBlockWrap.getElementsByClassName('aroundBlock')[0];
    aroundBlock.style.background = '#00FF00';
    numberForTest = numberForTest + 1;
    if(numberForTest <= 4) {
      progressBlocks[numberForTest - 1].classList.remove('w50f-disable');
      progressLines[numberForTest - 1].classList.remove('w50f-disable');
    }
  }

  if(bool){
    nextButton.style.display = 'block';
  }

  nextButton.onclick = () => {
    numberOfTest.innerHTML = numberForTest + 1;
    nextButton.style.display = 'none';
    wrap.innerHTML = variantOfChoise[numberForTest - 1];
    trueVariantCountForGreenOrRedRoundBlock = 0;
    titlePlace.innerText = titles[numberForTest - 1];
    if(trueAnswer >= 3 && numberForTest > 4){
      progressBlocks[progressBlocks.length - 1].style.background = '#FF9E7F';
      testWrap.innerHTML = `
      <div class="succes">
      <div class="succes-header all-center">
        Share result
      </div>
      <div id="id">
    
      </div>
      <a href="../category.html" id="button2" class="succes-button">Back</a>
    </div>
      `;
      succesPhoto.src = "img/succes.png";
      succesPhoto.classList.add('success');
    }else if(trueAnswer < 3 && numberForTest > 4){
      progressBlocks[progressBlocks.length - 1].style.background = '#FF9E7F';
      testWrap.innerHTML = `
      <div class="succes">
      <div class="succes-header all-center">
        Share result
      </div>
      <div id="id">
  
      </div>
      
      <a href="../category.html" id="button2" class="succes-button">Back</a>
    </div>
      `;
     
    }
  }
  function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
  }
}
