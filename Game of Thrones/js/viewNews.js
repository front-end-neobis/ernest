(function(){
  let viewNews = document.getElementById('view-News'),
      blackScreen = document.getElementById('black-screen'),
    closeMenu = document.getElementById('close-menu')
      
  
  viewNews.onclick = () => {
    blackScreenActiveFunc('active');
  }

  closeMenu.onclick = () => {
    blackScreenActiveFunc('disable');
  }


  let blackScreenActiveFunc = (word) => {
    if(word === 'active'){
      blackScreen.style.display = 'block';
     
    }
    else if (word === 'disable'){
      blackScreen.style.opacity = 0;
      
      setTimeout(() => {
        blackScreen.style.display = 'none';
      },300);
    }
  }
})();